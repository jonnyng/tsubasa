# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120701184112) do

  create_table "grades", :force => true do |t|
    t.integer  "grade_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "level_data", :force => true do |t|
    t.integer  "player_id"
    t.integer  "level"
    t.integer  "next_exp"
    t.integer  "shoot"
    t.integer  "dribble"
    t.integer  "pass"
    t.integer  "block"
    t.integer  "tackle"
    t.integer  "cut"
    t.integer  "speed"
    t.integer  "power"
    t.integer  "technique"
    t.integer  "stamina"
    t.integer  "catching"
    t.integer  "punching"
    t.integer  "offense"
    t.integer  "defense"
    t.integer  "total_point"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "players", :force => true do |t|
    t.integer  "player_id"
    t.string   "name"
    t.string   "short_name"
    t.integer  "player_team_id"
    t.integer  "name_id"
    t.integer  "exp_type"
    t.boolean  "good_position_fw"
    t.boolean  "good_position_mf"
    t.boolean  "good_position_df"
    t.boolean  "good_position_gk"
    t.string   "player_title"
    t.integer  "grade_id"
    t.string   "join_comment"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "level_data_count", :default => 0
  end

  add_index "players", ["player_id"], :name => "index_players_on_player_id", :unique => true

  create_table "special_attack_slots", :force => true do |t|
    t.integer  "player_id"
    t.integer  "special_attack_id"
    t.integer  "slotnum"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "special_attack_slots", ["player_id", "special_attack_id"], :name => "index_special_attack_slots_on_player_id_and_special_attack_id", :unique => true

  create_table "special_attacks", :force => true do |t|
    t.integer  "special_attack_id"
    t.integer  "special_attack_unique_id"
    t.integer  "persona_id"
    t.string   "name"
    t.string   "skill_comment"
    t.string   "attack_type"
    t.string   "attack_type_english"
    t.integer  "cost"
    t.integer  "power"
    t.boolean  "is_broken"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "special_attacks", ["special_attack_id"], :name => "index_special_attacks_on_special_attack_id", :unique => true

  create_table "teams", :force => true do |t|
    t.integer  "team_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
