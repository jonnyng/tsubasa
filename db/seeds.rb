# encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'json'

capture_file = 'C:\Users\jon\Documents\tsubasa_capture.txt'
grade_file = 'C:\Users\jon\Documents\tsubasa_grade.txt'
team_file = 'C:\Users\jon\Documents\tsubasa_teams.txt'
moves_file = 'C:\Users\jon\Documents\tsubasa_moves.txt'

#Player.delete_all
# if File.exists? capture_file
#   player_array = IO.readlines(capture_file)
#   player_array.each do |player_json|
#     if !player_json.start_with? 'Player ID'
#       player = JSON.parse player_json
#       Player.where(:player_id => player["player_info"]["id"]).first_or_create(:name => player["player_info"]["name"], 
#         :player_title => player["player_info"]["player_title"], :short_name => player["player_info"]["short_name"],
#         :player_team_id => player["player_info"]["player_team_id"], :good_position_fw => player["player_info"]["good_position_fw"], 
#         :good_position_mf => player["player_info"]["good_position_mf"], :good_position_df => player["player_info"]["good_position_df"], 
#         :good_position_gk => player["player_info"]["good_position_gk"], :join_comment => player["player_info"]["join_comment"],
#         :grade_id => player["player_info"]["grade_id"])
#       player["levels"].each do |level, level_values|
#         LevelDatum.where(:player_id => player["player_info"]["id"], :level => level_values["level"]).first_or_create(:next_exp => level_values["next_exp"], 
#           :shoot => level_values["shoot"], :dribble => level_values["dribble"], 
#           :pass => level_values["pass"], :block => level_values["block"], :tackle => level_values["tackle"], 
#           :cut => level_values["cut"], :speed => level_values["speed"], :power => level_values["power"], 
#           :technique => level_values["technique"], :stamina => level_values["stamina"], :catching => level_values["catching"], 
#           :punching => level_values["panching"], :offense => level_values["offense"], :defense => level_values["defense"],
#           :total_point => level_values["total_point"])
#       end
#       player_id = player["player_info"]["id"]
#       (player["special_attack_array"]).each do |special_attack|
#         SpecialAttack.create(:special_attack_id => special_attack["id"], :name => special_attack["name"])
#         slot = SpecialAttackSlot.where(:special_attack_id => special_attack["id"], :player_id => player["player_info"]["id"])
#         if slot.empty?
#           SpecialAttackSlot.create(:special_attack_id => special_attack["id"], :player_id => player["player_info"]["id"],
#           :slotnum => special_attack["slot_num"])
#         end
#       end
#     end
#   end
#   #else
#   #  Player.create(:player_id => 1057, :name => "松山 光", :player_title => "不屈の闘士",
#   #	:short_name => "松山", :player_team_id => 43)
# end


#Grade.delete_all
#if File.exists? grade_file
#  grade_array = IO.readlines(grade_file)
#  grade_hash = JSON.parse grade_array[0]
#  grade_hash.each do |grade_id, grade_id_hash|
#    Grade.create(:grade_id => grade_id, :name => grade_id_hash[grade_id])
#  end
#end

if File.exists? team_file
  team_array = IO.readlines(team_file)
  team_hash = JSON.parse team_array[0]
  team_hash.each do |team_id, team_name|
    Teams.create(:team_id => team_id, :name => team_name)
  end
end






# if File.exists? moves_file
#   moves_array = IO.readlines(moves_file)
#   moves_array.each do |moves_json|
#     if !moves_json.start_with? 'Move ID'
#       current_move = JSON.parse moves_json
#       database_move = SpecialAttack.where(:special_attack_id => current_move["id"])
#       type_hash = { 2 => "シュート", 6 => "シュート (低空)", 8 => "シュート (高空)", 101 => "ドリブル", 4 => "パス", 3 => "ワンツー",
#       9 => "タックル", 10 => "ブロック", 11 => "パスカット", 20 => "キャッチ", 21 => "パンチ" }
#       english_type_hash = { 2 => "Shoot (Ground)", 6 => "Shoot (Low Air)", 8 => "Shoot (High Air)", 101 => "Dribble", 4 => "Pass",
#       3 => "One-Two Combo", 9 => "Tackle", 10 => "Block", 11 => "Passcut", 20 => "Catch", 21 => "Punch" }
#       power = 0
#       case current_move["category_id"]
#       when 2, 6, 8
#         power = current_move["shoot"]
#       when 101
#         power = current_move["dribble"]
#       when 4, 3
#         power = current_move["pass"]
#       when 9
#         power = current_move["tackle"]
#       when 10
#         power = current_move["block"]
#       when 11
#         power = current_move["cut"]
#       when 20
#         power = current_move["catching"]
#       when 21
#         power = current_move["panching"]
#       end
#       
#       if database_move[0].nil?
#         #Most likely a player move, create it
#         SpecialAttack.create(:special_attack_id => current_move["id"], :name => current_move["name"], 
#           :special_attack_unique_id => current_move["unique_id"], :persona_id => current_move["name_id"], 
#           :cost => current_move["cost"], :attack_type => type_hash[current_move["category_id"]], 
#           :attack_type_english => english_type_hash[current_move["category_id"]],
#           :power => power, :is_broken => current_move["is_broken"], :skill_comment => current_move["skill_comment"])
#       elsif database_move[0].skill_comment.nil?
#         database_move[0].update_attributes(:special_attack_unique_id => current_move["unique_id"], :persona_id => current_move["name_id"], 
#           :cost => current_move["cost"], :attack_type => type_hash[current_move["category_id"]], :attack_type_english => english_type_hash[current_move["category_id"]],
#           :power => power, :is_broken => current_move["is_broken"], :skill_comment => current_move["skill_comment"])
#       end
#     end
#   end
# end










#

