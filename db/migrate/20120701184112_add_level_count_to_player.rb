class AddLevelCountToPlayer < ActiveRecord::Migration
  def self.up
    add_column :players, :level_data_count, :integer, :default => 0
    
    Player.reset_column_information
    Player.find_each do |p|
      Player.reset_counters p.id, :level_datum
    end
  end
  
  def self.down
    remove_column :players, :level_data_count
  end
end
