class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.integer :player_id
      t.string :name
      t.string :short_name
      t.integer :player_team_id
      t.integer :name_id
      t.integer :exp_type
      t.boolean :good_position_fw
      t.boolean :good_position_mf
      t.boolean :good_position_df
      t.boolean :good_position_gk
      t.string :player_title
      t.integer :grade_id
      t.string :join_comment

      t.timestamps
    end
    add_index :players, :player_id, :unique => true
  end
end
