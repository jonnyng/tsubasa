class ModifySpecialAttackType < ActiveRecord::Migration
  def change
    rename_column :special_attacks, :type, :attack_type
    rename_column :special_attacks, :type_english, :attack_type_english
  end
end
