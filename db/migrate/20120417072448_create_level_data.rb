class CreateLevelData < ActiveRecord::Migration
  def change
    create_table :level_data do |t|
      t.integer :player_id
      t.integer :level
      t.integer :next_exp
      t.integer :shoot
      t.integer :dribble
      t.integer :pass
      t.integer :block
      t.integer :tackle
      t.integer :cut
      t.integer :speed
      t.integer :power
      t.integer :technique
      t.integer :stamina
      t.integer :catching
      t.integer :punching
      t.integer :offense
      t.integer :defense
      t.integer :total_point

      t.timestamps
    end
  end
end
