class CreateSpecialAttacks < ActiveRecord::Migration
  def change
    create_table :special_attacks do |t|
      t.integer :special_attack_id
      t.integer :special_attack_unique_id
      t.integer :persona_id
      t.string :name
      t.string :skill_comment
      t.string :type
      t.string :type_english
      t.integer :cost
      t.integer :power
      t.boolean :is_broken

      t.timestamps
    end
    add_index :special_attacks, :special_attack_id, :unique => true
  end
end
