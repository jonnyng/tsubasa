class CreateSpecialAttackSlots < ActiveRecord::Migration
  def change
    create_table :special_attack_slots do |t|
      t.integer :player_id
      t.integer :special_attack_id
      t.integer :slotnum

      t.timestamps
    end
    add_index :special_attack_slots, [:player_id, :special_attack_id]
  end
end
