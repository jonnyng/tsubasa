class ModifyAttackSlotsIndex < ActiveRecord::Migration
  def change
    remove_index :special_attack_slots, [:player_id, :special_attack_id]
    add_index :special_attack_slots, [:player_id, :special_attack_id], :unique => true
  end
end
