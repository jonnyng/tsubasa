class SpecialAttackSlot < ActiveRecord::Base
  belongs_to :player, :primary_key => "player_id", :foreign_key => "player_id"
  belongs_to :special_attack, :primary_key => "special_attack_id", :foreign_key => "special_attack_id"
  
  delegate :name, :to => :special_attacks
end
