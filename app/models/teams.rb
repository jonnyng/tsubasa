class Teams < ActiveRecord::Base
  belongs_to :player, :foreign_key => "player_team_id"
  
  validates_presence_of :team_id
  validates_uniqueness_of :team_id
end
