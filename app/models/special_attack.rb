class SpecialAttack < ActiveRecord::Base
  has_many :special_attack_slots, :primary_key => "special_attack_id", :foreign_key => "special_attack_id"
  has_many :players, :through => :special_attack_slots, :primary_key => "special_attack_id", :foreign_key => "special_attack_id"
  
  validates_presence_of :special_attack_id
  validates_uniqueness_of :special_attack_id

  default_scope :order => "special_attack_id"
  
  def self.with_same_name
    where :name => self.name, :conditions => ["id != ?", self.id]
  end
  
  def to_param
    special_attack_id
  end
end
