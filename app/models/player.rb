class Player < ActiveRecord::Base
  has_many :special_attack_slots, :primary_key => "player_id", :foreign_key => "player_id"
  has_many :special_attacks, :through => :special_attack_slots, :primary_key => "player_id", :foreign_key => "player_id"
  has_many :level_datum, :primary_key => "player_id", :foreign_key => "player_id"
  has_one :max_level, :class_name => "LevelDatum", :conditions => {:next_exp => 0}, :primary_key => "player_id", :foreign_key => "player_id"
  has_one :grade, :primary_key => "grade_id", :foreign_key => "grade_id"
  has_one :teams, :primary_key => "player_team_id", :foreign_key => "team_id"
  
  validates_presence_of :player_id
  validates_uniqueness_of :player_id
  
  default_scope :order => "player_id"
  
  def self.with_grade(grade)
    if (grade.nil?)
      all
    else
      find_all_by_grade_id(grade)
    end
  end
    
  def to_param
    player_id
  end
end
