class Grade < ActiveRecord::Base
  belongs_to :player
  
  validates_presence_of :grade_id
  validates_uniqueness_of :grade_id
end
