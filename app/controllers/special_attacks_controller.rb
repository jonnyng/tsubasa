class SpecialAttacksController < ApplicationController
  def index
    @special_attack = SpecialAttack.all
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @special_attack }
    end
  end

  def show
    @special_attack = SpecialAttack.includes({:players => [:teams, :grade]}).find_by_special_attack_id(params[:id])

    respond_to do |format|
      format.html # show.html.haml
      format.json { render json: @special_attack }
    end
  end
end
