class LevelDataController < ApplicationController
  # GET /level_data
  # GET /level_data.json
  def index
    @level_data = LevelDatum.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @level_data }
    end
  end

  # GET /level_data/1
  # GET /level_data/1.json
  def show
    @level_datum = LevelDatum.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @level_datum }
    end
  end

  # GET /level_data/new
  # GET /level_data/new.json
  def new
    @level_datum = LevelDatum.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @level_datum }
    end
  end

  # GET /level_data/1/edit
  def edit
    @level_datum = LevelDatum.find(params[:id])
  end

  # POST /level_data
  # POST /level_data.json
  def create
    @level_datum = LevelDatum.new(params[:level_datum])

    respond_to do |format|
      if @level_datum.save
        format.html { redirect_to @level_datum, notice: 'Level datum was successfully created.' }
        format.json { render json: @level_datum, status: :created, location: @level_datum }
      else
        format.html { render action: "new" }
        format.json { render json: @level_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /level_data/1
  # PUT /level_data/1.json
  def update
    @level_datum = LevelDatum.find(params[:id])

    respond_to do |format|
      if @level_datum.update_attributes(params[:level_datum])
        format.html { redirect_to @level_datum, notice: 'Level datum was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @level_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /level_data/1
  # DELETE /level_data/1.json
  def destroy
    @level_datum = LevelDatum.find(params[:id])
    @level_datum.destroy

    respond_to do |format|
      format.html { redirect_to level_data_url }
      format.json { head :no_content }
    end
  end
end
