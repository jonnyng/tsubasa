require 'test_helper'

class LevelDataControllerTest < ActionController::TestCase
  setup do
    @level_datum = level_data(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:level_data)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create level_datum" do
    assert_difference('LevelDatum.count') do
      post :create, level_datum: @level_datum.attributes
    end

    assert_redirected_to level_datum_path(assigns(:level_datum))
  end

  test "should show level_datum" do
    get :show, id: @level_datum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @level_datum
    assert_response :success
  end

  test "should update level_datum" do
    put :update, id: @level_datum, level_datum: @level_datum.attributes
    assert_redirected_to level_datum_path(assigns(:level_datum))
  end

  test "should destroy level_datum" do
    assert_difference('LevelDatum.count', -1) do
      delete :destroy, id: @level_datum
    end

    assert_redirected_to level_data_path
  end
end
